package com.epam;

public class StashClientException extends RuntimeException{
    public StashClientException(int code, String reason) {
        super(reason);
    }
}
