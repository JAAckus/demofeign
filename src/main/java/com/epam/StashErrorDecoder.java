package com.epam;

import feign.Response;
import feign.codec.ErrorDecoder;

import static feign.FeignException.errorStatus;


public class StashErrorDecoder implements ErrorDecoder {

        @Override
        public Exception decode(String methodKey, Response response) {
            if (response.status() >= 400 && response.status() <= 499) {
                System.out.println("400");
                return new StashClientException(
                        response.status(),
                        response.body().toString()
                );
            }
            if (response.status() >= 500 && response.status() <= 599) {
                System.out.println("500");
                return new StashServerException(
                        response.status(),
                        response.body().toString()
                );
            }
            return errorStatus(methodKey, response);
        }
    }

