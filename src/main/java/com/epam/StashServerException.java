package com.epam;

public class StashServerException extends RuntimeException{
    public StashServerException(int code, String reason) {
        super(reason);
    }
}
