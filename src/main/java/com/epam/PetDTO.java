package com.epam;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PetDTO {
    private long id;
    private String name;
    private String status;
}
