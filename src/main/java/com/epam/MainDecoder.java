package com.epam;

import feign.FeignException;
import feign.Response;
import feign.codec.DecodeException;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;

import java.io.IOException;
import java.lang.reflect.Type;

import static feign.FeignException.errorStatus;


public class MainDecoder implements Decoder {


    @Override
    public Object decode(Response response, Type type) throws IOException, DecodeException, FeignException {
        if(response.status()!=200){
            return response.toString();
        }
        return null;
    }
}

