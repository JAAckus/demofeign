package com.epam;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import feign.Feign;
import feign.jackson.JacksonEncoder;

public class RestClient {


    public ApplicationEndpoints createClient() {
        ObjectMapper mapper = createObjectMapper();


        Feign.Builder feignBuilder = Feign.builder()
                .encoder(new JacksonEncoder(mapper)).decode404().decoder(new MainDecoder());



        return feignBuilder
                .target(ApplicationEndpoints.class, "https://petstore.swagger.io/v2");
    }

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }


}
