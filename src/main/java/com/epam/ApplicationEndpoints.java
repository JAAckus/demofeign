package com.epam;

import feign.Headers;
import feign.RequestLine;

public interface ApplicationEndpoints {

    @Headers({"Content-Type: application/json"})
    @RequestLine("POST /pet")
    PetDTO postPetDto(PetDTO body);


    @Headers({"Content-Type: application/json"})
    @RequestLine("POST /pet")
    String postPet(PetDTO body);

    @Headers({"Content-Type: application/json"})
    @RequestLine("POST /pet")
    String postPet(String body);
}
