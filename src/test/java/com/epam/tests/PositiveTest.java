package com.epam.tests;

import com.epam.ApplicationEndpoints;
import com.epam.PetDTO;
import com.epam.RestClient;
import com.epam.StashServerException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PositiveTest {

    private RestClient restClient = new RestClient();
    private ApplicationEndpoints applicationRestClient = restClient.createClient();

    @Test(groups = "positive")
    public void firstPositiveTestDTO(){

        PetDTO petDTO = PetDTO.builder().id(2345).name("ralf").status("available").build();
        PetDTO petDTOResponse = applicationRestClient.postPetDto(petDTO);
        Assert.assertEquals(petDTOResponse.getName(), "ralf", "wrong name");
    }


    @Test(expectedExceptions = StashServerException.class, groups = "negative")
    public void negative(){
            String petDTOResponse = applicationRestClient.postPet("{json}");
            System.out.println(petDTOResponse);
    }

    @Test(groups = "negative")
    public void negative2(){
        String petDTOResponse = applicationRestClient.postPet("{json}");
        System.out.println(petDTOResponse);
    }

    @Test(groups = "positive")
    public void firstPositiveTestString(){

        PetDTO petDTO = PetDTO.builder().id(234554).name("ralf2").status("available").build();
        String petDTOResponse = applicationRestClient.postPet("{json}");

    }
}
